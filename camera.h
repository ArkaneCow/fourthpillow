#ifndef CAMERA_H
#define CAMERA_H

#include "vector3.h"

class camera
{
public:
    camera(int width, int height, float fov);
    camera(int width, int height, float fov, vector3 pos, vector3 forward, vector3 up);
    int get_width();
    int get_height();
    float get_fov();
    vector3 get_forward();
    vector3 get_position();
    vector3 get_up();
    void set_forward(vector3 v);
    void set_position(vector3 v);
    void set_up(vector3 v);
private:
    int width;
    int height;
    float fov;
    vector3 position;
    vector3 forward;
    vector3 up;
};

#endif // CAMERA_H
