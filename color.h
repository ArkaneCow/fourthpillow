#ifndef COLOR_H
#define COLOR_H

class color
{
public:
    color();
    color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
    color(int red, int green, int blue, int alpha);
    color(float red, float green, float blue, float alpha);

    void set_red(unsigned char c);
    void set_green(unsigned char c);
    void set_blue(unsigned char c);
    void set_alpha(unsigned char c);

    void set_red(int i);
    void set_green(int i);
    void set_blue(int i);
    void set_alpha(int i);

    void set_red(float f);
    void set_green(float f);
    void set_blue(float f);
    void set_alpha(float f);

    unsigned char get_red();
    unsigned char get_green();
    unsigned char get_blue();
    unsigned char get_alpha();

    void add(color c);
    void subtract(color c);
    void multiply(float f);
    void divide(float f);

    virtual ~color();
private:
    unsigned char red;
    unsigned char green;
    unsigned char blue;
    unsigned char alpha;
};

#endif // COLOR_H
