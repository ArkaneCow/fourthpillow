#include "octree.h"

octree::octree()
{
    vector3 origin = vector3(0, 0, 0);
    vector3 half_dimension = vector3(0.5f, 0.5f, 0.5f);
    this->origin = origin;
    this->half_dimension = half_dimension;
    this->data = NULL;
    for (int i = 0; i < 8; i++) {
        this->children[i] = NULL;
    }
}

octree::octree(vector3 origin, vector3 half_dimension) {
    this->origin = origin;
    this->half_dimension = half_dimension;
    this->data = NULL;
    for (int i = 0; i < 8; i++) {
        this->children[i] = NULL;
    }
}

vector3 octree::get_origin() {
    return this->origin;
}

vector3 octree::get_half_dimension() {
    return this->half_dimension;
}

vector3 octree::get_full_dimension() {
    vector3 fd = this->half_dimension;
    fd.multiply(2.f);
    return fd;
}

point* octree::get_data() {
    return this->data;
}

octree** octree::get_children() {
    return this->children;
}

int octree::get_octant(vector3 point) {
    int oct = 0;
    if (point.get_x() >= this->origin.get_x()) {
        oct |= 4;
    }
    if (point.get_y() >= this->origin.get_y()) {
        oct |= 2;
    }
    if (point.get_z() >= this->origin.get_z()) {
        oct |= 1;
    }
    return oct;
}

bool octree::is_leaf() {
    return this->children[0] == NULL;
}

void octree::insert(point* p) {
    if (this->is_leaf()) {
        if (this->data == NULL) {
            data = p;
            return;
        } else {
            point* old = this->data;
            this->data = NULL;
            for (int i = 0; i < 8; i++) {
                vector3 new_origin = origin;
                new_origin.set_x(new_origin.get_x() + this->half_dimension.get_x() * (i&4 ? .5f : -.5f));
                new_origin.set_y(new_origin.get_y() + this->half_dimension.get_y() * (i&2 ? .5f : -.5f));
                new_origin.set_z(new_origin.get_z() + this->half_dimension.get_z() * (i&1 ? .5f : -.5f));
                vector3 new_half_dimension = this->half_dimension;
                new_half_dimension.multiply(0.5f);
                this->children[i] = new octree(new_origin, new_half_dimension);
            }
            this->children[this->get_octant(old->get_position())]->insert(old);
            this->children[this->get_octant(p->get_position())]->insert(p);
        }
    } else {
        int octant = this->get_octant(p->get_position());
        this->children[octant]->insert(p);
    }
}

void octree::get_all_points(std::vector<point *> &results) {
    if (this->is_leaf()) {
        if (this->data != NULL) {
            results.push_back(data);
        } else {
            for (int i = 0; i < 8; i++) {
                this->children[i]->get_all_points(results);
            }
        }
    }
}

void octree::get_box_points(vector3 min, vector3 max, std::vector<point*> &results) {
    if (this->is_leaf()) {
        if (this->data != NULL) {
            vector3 p = data->get_position();
            if (p.get_x() > max.get_x() || p.get_y() > max.get_y() || p.get_z() > max.get_z()) {
                return;
            }
            if (p.get_x() < min.get_x() || p.get_y() < min.get_y() || p.get_z() < min.get_z()) {
                return;
            }
            results.push_back(data);
        }
    } else {
        for (int i = 0; i < 8; i++) {
            vector3 cmax = this->children[i]->get_origin();
            cmax.add(this->children[i]->get_half_dimension());
            vector3 cmin = this->children[i]->get_origin();
            cmin.subtract(this->children[i]->get_half_dimension());
            if (cmax.get_x() < min.get_x() || cmax.get_y() < min.get_y() || cmax.get_z() < min.get_z()) {
                continue;
            }
            if (cmin.get_x() > max.get_x() || cmin.get_y() > max.get_y() || cmin.get_z() > max.get_z()) {
                continue;
            }
            this->children[i]->get_box_points(min, max, results);
        }
    }
}

void octree::from_bad_file(const char *filename) {
    using namespace std;
    int points = 0;
    string line_buff;
    ifstream bad_file(filename);
    if (bad_file.is_open()) {
        while (getline(bad_file, line_buff)) {
            char extra[] = "[]";
            line_buff.erase(std::remove(line_buff.begin(), line_buff.end(), extra[0]), line_buff.end());
            line_buff.erase(std::remove(line_buff.begin(), line_buff.end(), extra[1]), line_buff.end());
            int line_length;
            char** line_split = putils::split_string((char*) line_buff.c_str(), (char*) "/", &line_length);
            if (line_length != 3) {
                cout << "Invalid format\n";
                exit(1);
            }
            string s_position = string(line_split[0]);
            string s_normal = string(line_split[1]);
            string s_color = string(line_split[2]);
            int p_length;
            char** pos_split = putils::split_string((char*) s_position.c_str(), (char*) ",", &p_length);
            if (p_length != 3) {
                cout << "Invalid position\n";
                exit(1);
            }
            int n_length;
            char** norm_split = putils::split_string((char*) s_normal.c_str(), (char*) ",", &n_length);
            if (n_length != 3) {
                cout << "Invalid normal\n";
                exit(1);
            }
            int c_length;
            char** col_split = putils::split_string((char*) s_color.c_str(), (char*) ",", &c_length);
            if (c_length != 4) {
                cout << "Invalid color\n";
                exit(1);
            }
            float p_x = atof(pos_split[0]);
            float p_y = atof(pos_split[1]);
            float p_z = atof(pos_split[2]);
            float n_x = atof(norm_split[0]);
            float n_y = atof(norm_split[1]);
            float n_z = atof(norm_split[2]);
            int c_r = atoi(col_split[0]);
            int c_g = atoi(col_split[1]);
            int c_b = atoi(col_split[2]);
            int c_a = atoi(col_split[3]);
            vector3 position = vector3(p_x, p_y, p_z);
            vector3 normal = vector3(n_x, n_y, n_z);
            color col = color(c_r, c_g, c_b, c_a);
            point* p = new point(position, normal, col);
            this->insert(p);
            points++;
        }
        bad_file.close();
    } else {
        cout << "Unable to open file " << filename << "\n";
        exit(-1);
    }
    cout << "Points inserted: " << points << "\n";
    cout << "Finished constructing from file\n";
}

float* octree::serialize_all(int *o_length) {
    using namespace std;
    vector<float>* data = new vector<float>();
    octree::serialize(this, *data);
    *o_length = data->size();
    return &data[0][0];
}

int octree::serialize(octree *o, std::vector<float> &data) {
    //taking inefficiency to the extreme...
    int start_index = data.size();
    data.push_back(o->get_origin().get_x());
    data.push_back(o->get_origin().get_y());
    data.push_back(o->get_origin().get_z());
    data.push_back(o->get_half_dimension().get_x());
    data.push_back(o->get_half_dimension().get_y());
    data.push_back(o->get_half_dimension().get_z());
    if (o->get_data() != NULL) {
        data.push_back(o->get_data()->get_position().get_x());
        data.push_back(o->get_data()->get_position().get_y());
        data.push_back(o->get_data()->get_position().get_z());
        data.push_back(o->get_data()->get_normal().get_x());
        data.push_back(o->get_data()->get_normal().get_y());
        data.push_back(o->get_data()->get_normal().get_z());
        data.push_back((float) o->get_data()->get_color().get_red());
        data.push_back((float) o->get_data()->get_color().get_green());
        data.push_back((float) o->get_data()->get_color().get_blue());
        data.push_back((float) o->get_data()->get_color().get_alpha());
    } else {
        for (int i = 0; i < 10; i++) {
            data.push_back(NULL);
        }
    }
    for (int i = 0; i < 8; i++) {
        if (o->get_children()[i] != NULL) {
            data.push_back((float) octree::serialize(o->get_children()[i], data));
        } else {
            data.push_back(NULL);
        }
    }
    return start_index;
}
