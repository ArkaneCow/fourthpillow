#ifndef RAY_H
#define RAY_H

#include "vector3.h"

class ray
{
public:
    ray();
    ray(vector3 origin, vector3 direction);
    vector3 get_origin();
    vector3 get_direction();
    void set_origin(vector3 v);
    void set_direction(vector3 v);
private:
    vector3 origin;
    vector3 direction;
};

#endif // RAY_H
