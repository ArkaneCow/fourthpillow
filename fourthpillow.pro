TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
# CONFIG -= qt

QT += core gui widgets

SOURCES += main.cpp \
    octree.cpp \
    vector3.cpp \
    quaternion.cpp \
    point.cpp \
    putils.cpp \
    color.cpp \
    ray.cpp \
    engine.cpp \
    renderer_revelles_cpu.cpp \
    renderer_revelles_gpu.cpp \
    renderer_matrix_cpu.cpp \
    camera.cpp \
    world.cpp

# Windows
win32 {
# NVIDIA OpenCL
    #LIBS += -L"c:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v6.0/lib/Win32" -lOpenCL
    #INCLUDEPATH += "c:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v6.0/include"
    #DEPENDPATH += "c:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v6.0/include"
}

HEADERS += \
    octree.h \
    vector3.h \
    quaternion.h \
    point.h \
    putils.h \
    renderer.h \
    color.h \
    ray.h \
    engine.h \
    renderer_revelles_cpu.h \
    renderer_revelles_gpu.h \
    renderer_matrix_cpu.h \
    camera.h \
    world.h
