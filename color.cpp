#include "color.h"

color::color()
{
    this->red = (unsigned char) 0;
    this->green = (unsigned char) 0;
    this->blue = (unsigned char) 0;
    this->alpha = (unsigned char) 0;
}

color::color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) {
    this->red = red;
    this->green = green;
    this->blue = blue;
    this->alpha = alpha;
}

color::color(int red, int green, int blue, int alpha) {
    this->red = (unsigned char) red;
    this->green = (unsigned char) green;
    this->blue = (unsigned char) blue;
    this->alpha = (unsigned char) alpha;
}

color::color(float red, float green, float blue, float alpha) {
    this->red = (unsigned char) (red * 255);
    this->green = (unsigned char) (green * 255);
    this->blue = (unsigned char) (blue * 255);
    this->alpha = (unsigned char) (alpha * 255);
}

void color::set_red(unsigned char c) {
    this->red = c;
}

void color::set_green(unsigned char c) {
    this->green = c;
}

void color::set_blue(unsigned char c) {
    this->blue = c;
}

void color::set_alpha(unsigned char c) {
    this->alpha = c;
}

void color::set_red(int i) {
    this->red = (unsigned char) i;
}

void color::set_green(int i) {
    this->green = (unsigned char) i;
}

void color::set_blue(int i) {
    this->blue = (unsigned char) i;
}

void color::set_alpha(int i) {
    this->alpha = (unsigned char) i;
}

void color::set_red(float f) {
    this->red = (unsigned char) (f * 255);
}

void color::set_green(float f) {
    this->green = (unsigned char) (f * 255);
}

void color::set_blue(float f) {
    this->blue = (unsigned char) (f * 255);
}

void color::set_alpha(float f) {
    this->alpha = (unsigned char) (f * 255);
}

unsigned char color::get_red() {
    return this->red;
}

unsigned char color::get_green() {
    return this->green;
}

unsigned char color::get_blue() {
    return this->blue;
}

unsigned char color::get_alpha() {
    return this->alpha;
}

void color::add(color c) {
    int n_r = (int) this->red + (int) c.get_red();
    int n_g = (int) this->green + (int) c.get_green();
    int n_b = (int) this->blue + (int) c.get_blue();
    int n_a = (int) this->alpha + (int) c.get_alpha();
    this->red = (unsigned char) n_r;
    this->green = (unsigned char) n_g;
    this->blue = (unsigned char) n_b;
    this->alpha = (unsigned char) n_a;
}

void color::subtract(color c) {
    int n_r = (int) this->red - (int) c.get_red();
    int n_g = (int) this->green - (int) c.get_green();
    int n_b = (int) this->blue - (int) c.get_blue();
    int n_a = (int) this->alpha - (int) c.get_alpha();
    this->red = (unsigned char) n_r;
    this->green = (unsigned char) n_g;
    this->blue = (unsigned char) n_b;
    this->alpha = (unsigned char) n_a;
}

void color::multiply(float f) {
    int n_r = (int) this->red * f;
    int n_g = (int) this->green * f;
    int n_b = (int) this->blue * f;
    int n_a = (int) this->alpha * f;
    this->red = (unsigned char) n_r;
    this->green = (unsigned char) n_g;
    this->blue = (unsigned char) n_b;
    this->alpha = (unsigned char) n_a;
}

void color::divide(float f) {
    int n_r = (int) this->red / f;
    int n_g = (int) this->green / f;
    int n_b = (int) this->blue / f;
    int n_a = (int) this->alpha / f;
    this->red = (unsigned char) n_r;
    this->green = (unsigned char) n_g;
    this->blue = (unsigned char) n_b;
    this->alpha = (unsigned char) n_a;
}

color::~color() {

}
