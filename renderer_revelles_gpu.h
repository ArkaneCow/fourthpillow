#ifndef RENDERER_REVELLES_GPU_H
#define RENDERER_REVELLES_GPU_H

#include "renderer.h"
#include "world.h"

class renderer_revelles_gpu : public renderer
{
public:
    renderer_revelles_gpu();
    void render(unsigned char* buffer);
};

#endif // RENDERER_REVELLES_GPU_H
