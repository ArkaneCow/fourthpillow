#ifndef RENDERER_H
#define RENDERER_H

class renderer
{
public:
    virtual ~renderer() {}
    virtual void render(unsigned char* buffer) = 0;
};

#endif // RENDERER_H
