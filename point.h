#ifndef POINT_H
#define POINT_H

#include "vector3.h"
#include "color.h"

class point
{
public:
    point();
    point(vector3 pos, vector3 norm, color col);
    vector3 get_position();
    void set_position(vector3 position);
    vector3 get_normal();
    void set_normal(vector3 normal);
    color get_color();
    void set_color(color c);
private:
    vector3 p_position;
    vector3 p_normal;
    color p_color;
};

#endif // POINT_H
