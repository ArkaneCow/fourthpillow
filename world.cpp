#include "world.h"

camera* world::main_cam;
octree* world::object;

camera* world::get_main_cam() {
    return world::main_cam;
}

void world::init_main_cam(int width, int height, float fov) {
    world::main_cam = new camera(width, height, fov);
}

void world::init_main_cam(int width, int height, float fov, vector3 pos, vector3 forward, vector3 up) {
    world::main_cam = new camera(width, height, fov);
    world::main_cam->set_position(pos);
    world::main_cam->set_forward(forward);
    world::main_cam->set_up(up);
}

octree* world::get_object() {
    return world::object;
}

void world::set_object(octree *o) {
    world::object = o;
}
