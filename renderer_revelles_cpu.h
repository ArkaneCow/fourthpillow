#ifndef RENDERER_REVELLES_CPU_H
#define RENDERER_REVELLES_CPU_H

#include "renderer.h"
#include "world.h"
#include "octree.h"
#include "ray.h"

class renderer_revelles_cpu : public renderer
{
public:
    renderer_revelles_cpu();
    void render(unsigned char* buffer);
private:
    void calculate_angles();
    //http://stackoverflow.com/questions/10228690/ray-octree-intersection-algorithms Jeroen Baert octree traversal code
    void ray_parameter(octree* oct, ray r);
    void proc_subtree(float tx0, float ty0, float tz0, float tx1, float ty1, float tz1, octree* node);
    int new_node(float txm, int x, float tym, int y, float tzm, int z);
    int first_node(float tx0, float ty0, float tz0, float txm, float txy, float tzm);
    void proc_terminal(octree* o);

    int render_width;
    int render_height;
    vector3 cam_pos;
    vector3 cam_dir;
    vector3 cam_up;
    float fov;
    unsigned char a;
    int current_x;
    int current_y;
    unsigned char* frame;
    float* angle_array;

};

#endif // RENDERER_REVELLES_CPU_H
