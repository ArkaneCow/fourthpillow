#ifndef ENGINE_H
#define ENGINE_H

#include <QWidget>
#include <QTimer>
#include <QPainter>
#include <QImage>
#include <QDebug>
#include <QKeyEvent>

#include "putils.h"
#include "octree.h"
#include "renderer.h"
#include "world.h"

#include "renderer_revelles_cpu.h"

class engine : public QWidget
{
public:
    engine(int width, int height, int frame_rate);
protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent* event);
private:
    void init();
    void startup();

    QImage frame;
    QPainter frame_painter;

    renderer* render;

    int interval;
    int time_elapsed;
    int frame_rate;

    int width;
    int height;
    unsigned char* buffer;

    int frame_count;

    void draw_frame();
    void print_fps();
    octree* test;
};

#endif // ENGINE_H
