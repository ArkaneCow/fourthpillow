#include "camera.h"

camera::camera(int width, int height, float fov)
{
    this->width = width;
    this->height = height;
    this->fov = fov;
    vector3 forward = vector3(1, 0, 0);
    vector3 up = vector3(0, 1, 0);
    vector3 pos = vector3(0, 0, 0);
    this->forward = forward;
    this->up = up;
    this->position = pos;
}

camera::camera(int width, int height, float fov, vector3 pos, vector3 forward, vector3 up) {
    this->width = width;
    this->height = height;
    this->fov = fov;
    this->position = pos;
    this->forward = forward;
    this->up = up;
}

int camera::get_width() {
    return this->width;
}

int camera::get_height() {
    return this->height;
}

float camera::get_fov() {
    return this->fov;
}

vector3 camera::get_forward() {
    return this->forward;
}

vector3 camera::get_position() {
    return this->position;
}

vector3 camera::get_up() {
    return this->up;
}

void camera::set_forward(vector3 v) {
    this->forward = v;
}

void camera::set_position(vector3 v) {
    this->position = v;
}

void camera::set_up(vector3 v) {
    this->up = up;
}
