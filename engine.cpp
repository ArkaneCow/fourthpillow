#include "engine.h"

engine::engine(int width, int height, int frame_rate)
{
    this->frame_rate = frame_rate;
    this->time_elapsed = 0;
    this->frame_count = 0;
    this->interval = 1000 / this->frame_rate;
    this->width = width;
    this->height = height;
    qDebug() << "Setting window size";
    this->setFixedSize(this->width, this->height);
    qDebug() << "Creating byte frame buffer";
    this->buffer = new unsigned char[this->width * this->height * 3];
    for (int i = 0; i < this->width * this->height * 3; i++) {
        this->buffer[i] = (unsigned char) 255;
    }
    qDebug() << "Creating QImage frame buffer";
    this->frame = QImage(this->width, this->height, QImage::Format_RGB32);
    qDebug() << "Creating update timer";
    QTimer* update_timer = new QTimer(this);
    connect(update_timer, SIGNAL(timeout()), this, SLOT(update()));
    qDebug() << "Starting update timer";
    update_timer->start(this->interval);
    this->show();
    this->init();
    this->startup();
}

void engine::paintEvent(QPaintEvent *) {
    draw_frame();
    this->frame_count++;
    this->time_elapsed += this->interval;
    if (time_elapsed >= 1000) {
        print_fps();
        this->frame_count = 0;
        this->time_elapsed = 0;
    }
}

void engine::keyPressEvent(QKeyEvent *event) {
    qDebug() << event->key();
    if (event->key() == Qt::Key_Escape) {
        //exit engine
        exit(0);
    }
}

void engine::draw_frame() {
    /*
    int l_test;
    float* data = test->serialize_all(&l_test);
    qDebug() << l_test;
    delete data;
    */
    for (int i = 0; i < this->width * this->height * 3; i++) {
        this->buffer[i] = (unsigned char) 0;
    }
    this->render->render(this->buffer);
    frame_painter.begin(this);
    for (int x = 0; x < this->width; x++) {
        for (int y = 0; y < this->height; y++) {
            int pixel_index = putils::get_index_3d(x, y, 0, this->width, 3);
            frame.setPixel(x, y, qRgb(this->buffer[pixel_index], this->buffer[pixel_index + 1], this->buffer[pixel_index + 2]));
        }
    }
    frame_painter.drawImage(0, 0, frame);
    frame_painter.end();
}

void engine::print_fps() {
    qDebug() << "FPS: " << this->frame_count;
}

void engine::init() {
    qDebug() << "Initializing Engine";
    world::init_main_cam(this->width, this->height, (float) 70);
}

void engine::startup() {
    qDebug() << "Starting up";
    test = new octree(vector3(0, 0, 0), vector3(10, 10, 10));
    qDebug() << "Created test octree";
    test->from_bad_file("goose2.obj.bad");
    qDebug() << "loaded file";
    world::set_object(test);
    world::get_main_cam()->set_position(vector3(-10, -10, -10));
    this->render = new renderer_revelles_cpu();
}
