#include "renderer_revelles_cpu.h"

renderer_revelles_cpu::renderer_revelles_cpu()
{
    this->render_width = world::get_main_cam()->get_width();
    this->render_height = world::get_main_cam()->get_height();
    this->fov = world::get_main_cam()->get_fov();
    calculate_angles();
}

void renderer_revelles_cpu::render(unsigned char* buffer) {
    this->frame = buffer;
    this->render_width = world::get_main_cam()->get_width();
    this->render_height = world::get_main_cam()->get_height();
    this->fov = world::get_main_cam()->get_fov();
    this->cam_pos = world::get_main_cam()->get_position();
    this->cam_dir = world::get_main_cam()->get_forward();
    this->cam_up = world::get_main_cam()->get_up();
    for (int x = 0; x < this->render_width; x++) {
        for (int y = 0; y < this->render_height; y++) {
            this->current_x = x;
            this->current_y = y;
            vector3 p_dir = this->cam_dir;
            vector3 y_axis = this->cam_up;
            vector3 h_axis = y_axis;
            h_axis.cross_product(this->cam_dir);
            int angle_index = putils::get_index_3d(this->current_x, this->current_y, 0, this->render_width, 2);
            float offset_x = this->angle_array[angle_index];
            float offset_y = this->angle_array[angle_index + 1];
            p_dir.rotate(offset_x, y_axis);
            p_dir.rotate(offset_y, h_axis);
            ray p_ray = ray(this->cam_pos, p_dir);
            ray_parameter(world::get_object(), p_ray);
        }
    }
}

void renderer_revelles_cpu::calculate_angles() {
    float fov_x = this->fov;
    float ratio = (float) this->render_height / (float) this->render_width;
    float fov_y = ratio * fov_x;
    this->angle_array = new float[this->render_width * this->render_height * 2];
    float half_fov_x = fov_x / (float) 2;
    float half_fov_y = fov_y / (float) 2;
    for (int x = 0; x < this->render_width; x++) {
        for (int y = 0; y < this->render_height; y++) {
            float offset_x = putils::linear_interpolate(-half_fov_x, half_fov_x, (float) ((float) x / (float) this->render_width));
            float offset_y = putils::linear_interpolate(-half_fov_y, half_fov_y, (float) ((float) y / (float) this->render_height));
            int angle_index = putils::get_index_3d(x, y, 0, this->render_width, 2);
            this->angle_array[angle_index] = offset_x;
            this->angle_array[angle_index + 1] = offset_y;
        }
    }
}

//http://stackoverflow.com/questions/10228690/ray-octree-intersection-algorithms Jeroen Baert
int renderer_revelles_cpu::new_node(float txm, int x, float tym, int y, float tzm, int z) {
    if (txm < tym) {
        return x;
    } else {
        if (tym < tzm) {
            return y;
        }
    }
    return z;
}

int renderer_revelles_cpu::first_node(float tx0, float ty0, float tz0, float txm, float tym, float tzm) {
    unsigned char answer = 0;
    if (tx0 > ty0) {
        if (tx0 > tz0) {
            if (tym < tx0) {
                answer |= 2;
            }
            if (tzm < tx0) {
                answer |= 1;
            }
            return (int) answer;
        }
    } else {
        if (ty0 > tz0) {
            if (txm < ty0) {
                answer |= 4;
            }
            if (tzm < ty0) {
                answer |= 1;
            }
            return (int) answer;
        }
    }
    if (txm < tz0) {
        answer |= 4;
    }
    if (tym < tz0) {
        answer |= 2;
    }
    return (int) answer;
}

void renderer_revelles_cpu::ray_parameter(octree *oct, ray r) {
    this->a = 0;
    if (r.get_direction().get_x() < 0.0f) {
        vector3 r_origin = vector3(oct->get_half_dimension().get_x() * 2 - r.get_origin().get_x(), r.get_origin().get_y(), r.get_origin().get_z());
        r.set_origin(r_origin);
        vector3 r_direction = vector3(-r.get_direction().get_x(), r.get_direction().get_y(), r.get_direction().get_z());
        r.set_direction(r_direction);
        a |= 4;
    }
    if (r.get_direction().get_y() < 0.0f) {
        vector3 r_origin = vector3(r.get_origin().get_x(), oct->get_half_dimension().get_y() * 2 - r.get_origin().get_y(), r.get_origin().get_z());
        r.set_origin(r_origin);
        vector3 r_direction = vector3(r.get_direction().get_x(), -r.get_direction().get_y(), r.get_direction().get_z());
        r.set_direction(r_direction);
        a |= 2;
    }
    if (r.get_direction().get_z() < 0.0f) {
        vector3 r_origin = vector3(r.get_origin().get_x(), r.get_origin().get_y(), oct->get_half_dimension().get_z() * 2 - r.get_origin().get_z());
        r.set_origin(r_origin);
        vector3 r_direction = vector3(r.get_direction().get_x(), r.get_direction().get_y(), -r.get_direction().get_z());
        r.set_direction(r_direction);
        a |= 1;
    }
    float tx0 = (oct->get_origin().get_x() - oct->get_half_dimension().get_x()) / r.get_direction().get_x();
    float tx1 = (oct->get_origin().get_x() + oct->get_half_dimension().get_x()) / r.get_direction().get_x();
    float ty0 = (oct->get_origin().get_y() - oct->get_half_dimension().get_y()) / r.get_direction().get_y();
    float ty1 = (oct->get_origin().get_y() + oct->get_half_dimension().get_y()) / r.get_direction().get_y();
    float tz0 = (oct->get_origin().get_z() - oct->get_half_dimension().get_z()) / r.get_direction().get_z();
    float tz1 = (oct->get_origin().get_z() + oct->get_half_dimension().get_z()) / r.get_direction().get_z();
    if (fmaxf(fmaxf(tx0, ty0), tz0) < fminf(fminf(tx1, ty1), tz1)) {
        proc_subtree(tx0, ty0, tz0, tx1, ty1, tz1, oct);
    }
}

void renderer_revelles_cpu::proc_terminal(octree *o) {
    int pixel_index = putils::get_index_3d(this->current_x, this->current_y, 0, this->render_width, 3);
    //printf("x %d y %d z %d w %d t %d\n", this->current_x, this->current_y, 0, this->render_width, 3);
    if (o->get_data() != NULL) {
        //printf("pixel index is %d\n", pixel_index);
        this->frame[pixel_index] = o->get_data()->get_color().get_red();
        this->frame[pixel_index + 1] = o->get_data()->get_color().get_green();
        this->frame[pixel_index + 2] = o->get_data()->get_color().get_blue();
    } else {
        this->frame[pixel_index] = (unsigned char) 255;
        this->frame[pixel_index + 1] = (unsigned char) 0;
        this->frame[pixel_index + 2] = (unsigned char) 0;
    }
}

void renderer_revelles_cpu::proc_subtree(float tx0, float ty0, float tz0, float tx1, float ty1, float tz1, octree *node) {
    if (node == NULL) {
        return;
    }
    float txm, tym, tzm;
    int currNode;
    if (tx1 < 0.0f || ty1 < 0.0f || tz1 < 0.0f) {
        return;
    }
    if (node->is_leaf()) {
        proc_terminal(node);
        return;
    }
    txm = 0.5f * (tx0 + tx1);
    tym = 0.5f * (ty0 + ty1);
    tzm = 0.5f * (tz0 + tz1);
    currNode = first_node(tx0, ty0, tz0, txm, tym, tzm);
    do {
        switch (currNode) {
        case 0:
            proc_subtree(tx0, ty0, tz0, txm, tym, tzm, node->get_children()[a]);
            currNode = new_node(txm, 4, tym, 2, tzm, 1);
            break;
        case 1:
            proc_subtree(tx0, ty0, tzm, txm, tym, tz1, node->get_children()[1 ^ a]);
            currNode = new_node(txm, 5, tym, 3, tz1, 8);
            break;
        case 2:
            proc_subtree(tx0, tym, tz0, txm, ty1, tzm, node->get_children()[2 ^ a]);
            currNode = new_node(txm, 6, ty1, 8, tzm, 3);
            break;
        case 3:
            proc_subtree(tx0, tym, tzm, txm, ty1, tz1, node->get_children()[3 ^ a]);
            currNode = new_node(txm, 7, ty1, 8, tz1, 8);
            break;
        case 4:
            proc_subtree(txm, ty0, tz0, tx1, tym, tzm, node->get_children()[4 ^ a]);
            currNode = new_node(tx1, 8, tym, 6, tzm, 5);
            break;
        case 5:
            proc_subtree(txm, ty0, tzm, tx1, tym, tz1, node->get_children()[5 ^ a]);
            currNode = new_node(tx1, 8, tym, 7, tz1, 8);
            break;
        case 6:
            proc_subtree(txm, tym, tz0, tx1, ty1, tzm, node->get_children()[6 ^ a]);
            currNode = new_node(tx1, 8, ty1, 8, tzm, 7);
            break;
        case 7:
            proc_subtree(txm, tym, tzm, tx1, ty1, tz1, node->get_children()[7 ^ a]);
            currNode = 8;
            break;
        }
    } while (currNode < 8);
}
