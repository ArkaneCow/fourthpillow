#include "ray.h"

ray::ray()
{
    vector3 _origin = vector3();
    vector3 _direction = vector3();
    this->origin = _origin;
    this->direction = _direction;
}

ray::ray(vector3 origin, vector3 direction) {
    this->origin = origin;
    this->direction = direction;
}

vector3 ray::get_origin() {
    return this->origin;
}

vector3 ray::get_direction() {
    return this->direction;
}

void ray::set_origin(vector3 v) {
    this->origin = v;
}

void ray::set_direction(vector3 v) {
    this->direction = v;
}
