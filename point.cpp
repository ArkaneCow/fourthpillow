#include "point.h"

point::point()
{
    vector3 origin = vector3(0, 0, 0);
    this->p_position = origin;
}

point::point(vector3 pos, vector3 norm, color col) {
    this->p_position = pos;
    this->p_normal = norm;
    this->p_color = col;
}

vector3 point::get_position() {
    return this->p_position;
}

void point::set_position(vector3 position) {
    this->p_position = position;
}

vector3 point::get_normal() {
    return this->p_normal;
}

void point::set_normal(vector3 normal) {
    this->p_normal = normal;
}

color point::get_color() {
    return this->p_color;
}

void point::set_color(color c) {
    this->p_color = c;
}
