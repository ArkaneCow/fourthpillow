#ifndef WORLD_H
#define WORLD_H

#include "camera.h"
#include "octree.h"

class world
{
public:
    static void init_main_cam(int width, int height, float fov);
    static void init_main_cam(int width, int height, float fov, vector3 pos, vector3 forward, vector3 up);
    static camera* get_main_cam();
    static octree* get_object();
    static void set_object(octree* o);
private:
    static camera* main_cam;
    static octree* object;
};

#endif // WORLD_H
