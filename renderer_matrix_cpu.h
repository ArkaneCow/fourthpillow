#ifndef RENDERER_MATRIX_CPU_H
#define RENDERER_MATRIX_CPU_H

#include "renderer.h"
#include "world.h"

class renderer_matrix_cpu : public renderer
{
public:
    renderer_matrix_cpu();
    void render(unsigned char* buffer);
};

#endif // RENDERER_MATRIX_CPU_H
