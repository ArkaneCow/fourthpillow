#ifndef OCTREE_H
#define OCTREE_H

#include <vector>
#include <algorithm>
#include "point.h"
#include "putils.h"

class octree
{
public:
    octree();
    octree(vector3 origin, vector3 half_dimension);
    vector3 get_origin();
    vector3 get_half_dimension();
    vector3 get_full_dimension();
    point* get_data();
    octree** get_children();
    int get_octant(vector3 point);
    bool is_leaf();
    void insert(point* p);
    void get_all_points(std::vector<point*>& results);
    void get_box_points(vector3 min, vector3 max, std::vector<point*>& results);
    void from_bad_file(const char* filename);
    float* serialize_all(int* o_length);

    static int serialize(octree* o, std::vector<float>& data);
private:
    vector3 origin;
    vector3 half_dimension;
    octree* children[8];
    point* data;
};

#endif
